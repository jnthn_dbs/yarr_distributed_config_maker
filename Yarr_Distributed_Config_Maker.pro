#-------------------------------------------------
#
# Project created by QtCreator 2018-08-11T13:42:54
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = Yarr_Distributed_Config_Maker
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

SOURCES += \
    main.cpp \
    src/mainwindow.cpp \
    src/zmqtcpconnectivitywidget.cpp \
    src/rowawaregridlayout.cpp \
    src/tuningparameterwidget.cpp \
    src/scanwidget.cpp \
    src/hwcontrollerwidget.cpp \
    src/chipconfigswidget.cpp

HEADERS += \
    inc/mainwindow.hpp \
    inc/json.hpp \
    inc/zmqtcpconnectivitywidget.hpp \
    inc/rowawaregridlayout.hpp \
    inc/tuningparameterwidget.hpp \
    inc/scanwidget.hpp \
    inc/hwcontrollerwidget.hpp \
    inc/chipconfigswidget.hpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
