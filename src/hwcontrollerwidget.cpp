#include "inc/hwcontrollerwidget.hpp"

HwControllerWidget::HwControllerWidget(QWidget *parent)
    : QWidget(parent)
{
    auto hwControllerWidgetLayout = new QHBoxLayout;
    this->setLayout(hwControllerWidgetLayout);

    this->controllerConfigFilePathInput = new QLineEdit;

    auto browseFileSystemButton = new QPushButton("...");

    QObject::connect(
        browseFileSystemButton,
        &QPushButton::clicked,
        [this]()->void
            {
                this->controllerConfigFilePathInput->setText(
                    QFileDialog::getOpenFileName(
                        this, "HwController Config File", "", "JSON files (*.json);; All files (*)"
                    )
                );
            }
    );

    hwControllerWidgetLayout->addWidget(new QLabel("controllerConfigFilePath"));
    hwControllerWidgetLayout->addWidget(this->controllerConfigFilePathInput);
    hwControllerWidgetLayout->addWidget(browseFileSystemButton);
}

HwControllerWidget::~HwControllerWidget()
{
    ;
}

std::string HwControllerWidget::getControllerConfigFilePath() const
{
    return this->controllerConfigFilePathInput->text().toStdString();
}
