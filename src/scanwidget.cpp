#include "inc/scanwidget.hpp"

ScanWidget::ScanWidget(QWidget *parent)
    : QWidget(parent)
{
    auto scanWidgetLayout = new QHBoxLayout;
    this->setLayout(scanWidgetLayout);

    this->typeInput = new QLineEdit("digitalscan");

    scanWidgetLayout->addWidget(new QLabel("type"));
    scanWidgetLayout->addWidget(this->typeInput);
}

ScanWidget::~ScanWidget()
{
    ;
}

std::string ScanWidget::getType() const
{
    return this->typeInput->text().toStdString();
}
