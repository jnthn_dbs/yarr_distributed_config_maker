#include "inc/chipconfigswidget.hpp"

ChipConfigsWidget::ChipConfigsWidget(QWidget *parent)
    : QWidget(parent)
{
    auto chipConfigsWidgetLayout = new QGridLayout;
    this->setLayout(chipConfigsWidgetLayout);

    this->chipTypeInput = new QComboBox;
    this->chipTypeInput->addItems({"", "FE-I4B", "FE65-P2", "RD53A"});

    this->maskOptInput = new QSpinBox;
    this->maskOptInput->setMinimum(-1);
    this->maskOptInput->setMaximum(1);
    this->maskOptInput->setValue(-1);

    auto addFilePathInputPushButton = new QPushButton("+");

    QObject::connect(
        addFilePathInputPushButton,
        &QPushButton::clicked,
        [this]()->void
            {
                this->addFilePathInput();
            }
    );

    chipConfigsWidgetLayout->addWidget(new QLabel("chipType"), 0, 0);
    chipConfigsWidgetLayout->addWidget(new QLabel("maskOpt"), 1, 0);
    chipConfigsWidgetLayout->addWidget(new QLabel("filePathes"), 2, 0);

    chipConfigsWidgetLayout->addWidget(this->chipTypeInput, 0, 1);
    chipConfigsWidgetLayout->addWidget(this->maskOptInput, 1, 1);
    chipConfigsWidgetLayout->addWidget(addFilePathInputPushButton, 2, 2);

    this->addFilePathInput();

    this->setMinimumWidth(500);
}

ChipConfigsWidget::~ChipConfigsWidget()
{
    ;
}

std::string ChipConfigsWidget::getChipType() const
{
    return this->chipTypeInput->currentText().toStdString();
}

int ChipConfigsWidget::getMaskOpt() const
{
    return this->maskOptInput->value();
}

std::vector<std::string> ChipConfigsWidget::getFilePathes() const
{
    std::vector<std::string> filePathes;
    std::string filePathTmp;

    for (QLineEdit *lineEdit : this->filePathesInputList) {
        filePathTmp = lineEdit->text().toStdString();

        if (!filePathTmp.empty()) {
            filePathes.push_back(filePathTmp);
        }
    }

    return filePathes;
}

void ChipConfigsWidget::addFilePathInput(const QString &filePath)
{
    auto chipConfigsWidgetLayout = dynamic_cast<QGridLayout*>(this->layout());

    auto filePathInput = new QLineEdit;
    filePathInput->setText(filePath);
    this->filePathesInputList.append(filePathInput);

    auto browseFileSystemButton = new QPushButton("...");

    QObject::connect(
        browseFileSystemButton,
        &QPushButton::clicked,
        [filePathInput, this]()->void
            {
                QStringList openFileNames = QFileDialog::getOpenFileNames(
                    this, "Frontend Config Files", "", "JSON files (*.json);; All files (*)"
                );

                if (openFileNames.empty()) {
                    return;
                }

                filePathInput->setText(openFileNames.at(0));

                for (int i = 1, openFileNamesCount = openFileNames.count(); i < openFileNamesCount; ++i) {
                    this->addFilePathInput(openFileNames.at(i));
                }
            }
    );

    int rowCount = chipConfigsWidgetLayout->rowCount();

    chipConfigsWidgetLayout->addWidget(filePathInput, rowCount, 1);
    chipConfigsWidgetLayout->addWidget(browseFileSystemButton, rowCount, 2);
}
