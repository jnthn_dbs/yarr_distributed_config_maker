#include "inc/rowawaregridlayout.hpp"

RowAwareGridLayout::RowAwareGridLayout(QWidget *parent)
    : QGridLayout(parent)
{
    ;
}

RowAwareGridLayout::~RowAwareGridLayout()
{
    ;
}

int RowAwareGridLayout::filledRowCount()
{
    int columnCount = this->columnCount();

    for (int rowCount = this->rowCount(); rowCount > 0; --rowCount) {
        for (int column = 0; column < columnCount; ++column) {
            if (nullptr != this->itemAtPosition(rowCount - 1, column)) {
                return rowCount;
            }
        }
    }

    return 0;
}
