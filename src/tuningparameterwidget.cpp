#include "inc/tuningparameterwidget.hpp"

TuningParameterWidget::TuningParameterWidget(const QString &tuningParameter, QWidget *parent)
    : QWidget(parent)
{
    auto tuningParameterWidgetLayout = new QHBoxLayout;
    this->setLayout(tuningParameterWidgetLayout);

    this->tuningParameterInput = new QSpinBox;
    this->tuningParameterInput->setMaximum(std::numeric_limits<int>::max());

    tuningParameterWidgetLayout->addWidget(new QLabel(tuningParameter));
    tuningParameterWidgetLayout->addWidget(tuningParameterInput);
}

TuningParameterWidget::~TuningParameterWidget()
{
    ;
}

int TuningParameterWidget::getTuningParameter() const
{
    return this->tuningParameterInput->value();
}
