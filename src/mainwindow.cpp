#include "inc/mainwindow.hpp"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QMenu *saveMenu = this->menuBar()->addMenu("File");
    saveMenu->addAction("Save as");
    QObject::connect(saveMenu, SIGNAL(triggered(QAction*)), this, SLOT(fileActionSlot(QAction*)));

    auto centralWidget = new QScrollArea(this);
    auto scrollAreaWidget = new QWidget;
    this->centralWidgetLayout = new RowAwareGridLayout;
    this->centralWidgetLayout->setSizeConstraint(QLayout::SetFixedSize);

    scrollAreaWidget->setLayout(this->centralWidgetLayout);
    centralWidget->setWidget(scrollAreaWidget);
    this->setCentralWidget(centralWidget);

    this->addConfigTypeInput();
}

MainWindow::~MainWindow()
{
    ;
}

void MainWindow::addConfigTypeInput()
{
    auto configTypeInput = new QComboBox;
    configTypeInput->addItem("");

    int filledRowCount = this->centralWidgetLayout->filledRowCount();

    if (2 > filledRowCount) {
        configTypeInput->addItem("runConfig");
    }

    configTypeInput->addItems({"ScanSlave", "ProcessingSlave", "HistogrammingSlave", "AnalysisSlave"});

    QObject::connect(
        configTypeInput,
        &QComboBox::currentTextChanged,
        [this, configTypeInput](const QString &configType)->void
            {
                this->configTypeHandler(configTypeInput, configType);
            }
    );

    this->centralWidgetLayout->addWidget(configTypeInput, filledRowCount, 0);
}

void MainWindow::saveAsActionHandler()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save config", "", "JSON files (*.json);; All files (*)");

    if ("" == fileName) {
        return;
    }

    nlohmann::json json;
    this->parseWidgetTree(0, this->centralWidgetLayout->filledRowCount(), 0, json);

    QFile file(fileName);
    file.open(QIODevice::WriteOnly);

    QTextStream qTSFile(&file);
    qTSFile << QString::fromStdString(json.dump(4)) << '\n';

    file.close();

    QMessageBox saveAsInfo(QMessageBox::Information, "Save as", "File successfully saved", QMessageBox::Ok, this);
    saveAsInfo.exec();
}

void MainWindow::parseWidgetTree(int rowBegin, int rowEnd, int columnBegin, nlohmann::json &json)
{
    int nextItemRow = rowEnd;

    QLayoutItem *itemTmp;
    QWidget *itemTmpWidget;
    QString itemTmpWidgetClassName;

    std::string comboBoxCurrentText;
    ZmqTcpConnectivityWidget *zmqTcpConnectivityWidgetTmp;

    for (int row = rowBegin; row < rowEnd; ++row) {
        if (nextItemRow > row && nextItemRow < rowEnd) {
            row = nextItemRow;
            nextItemRow = rowEnd;
        }

        itemTmp = this->centralWidgetLayout->itemAtPosition(row, columnBegin);

        if (nullptr != itemTmp) {
            itemTmpWidget = itemTmp->widget();
            itemTmpWidgetClassName = itemTmpWidget->metaObject()->className();

            if ("QComboBox" == itemTmpWidgetClassName) {
                comboBoxCurrentText = dynamic_cast<QComboBox*>(itemTmpWidget)->currentText().toStdString();

                if (comboBoxCurrentText.empty()) {
                    continue;
                }

                json[comboBoxCurrentText] = nlohmann::json::object({});

                for (nextItemRow = row + 1; nextItemRow < rowEnd; ++nextItemRow) {
                    if (nullptr != this->centralWidgetLayout->itemAtPosition(nextItemRow, columnBegin)) {
                        break;
                    }
                }

                this->parseWidgetTree(row + 1, nextItemRow, columnBegin + 1, json[comboBoxCurrentText]);
            } else if ("ScanWidget" == itemTmpWidgetClassName) {
                json["type"] = dynamic_cast<ScanWidget*>(itemTmpWidget)->getType();
            } else if ("TuningParameterWidget" == itemTmpWidgetClassName) {
                json = dynamic_cast<TuningParameterWidget*>(itemTmpWidget)->getTuningParameter();
            } else if ("HwControllerWidget" == itemTmpWidgetClassName) {
                json["controllerConfigFilePath"] =
                    dynamic_cast<HwControllerWidget*>(itemTmpWidget)->getControllerConfigFilePath();
            } else if ("ChipConfigsWidget" == itemTmpWidgetClassName) {
                ChipConfigsWidget *chipConfigsWidget = dynamic_cast<ChipConfigsWidget*>(itemTmpWidget);

                json["chipType"] = chipConfigsWidget->getChipType();
                json["maskOpt"] = chipConfigsWidget->getMaskOpt();
                json["filePathes"] = chipConfigsWidget->getFilePathes();
            } else if ("ZmqTcpConnectivityWidget" == itemTmpWidgetClassName) {
                zmqTcpConnectivityWidgetTmp = dynamic_cast<ZmqTcpConnectivityWidget*>(itemTmpWidget);

                json["ipAddress"] = zmqTcpConnectivityWidgetTmp->getIpAddress();
                json["port"] = zmqTcpConnectivityWidgetTmp->getPort();
                json["bind"] = zmqTcpConnectivityWidgetTmp->getBind();
                json["receiveTimeout"] = zmqTcpConnectivityWidgetTmp->getReceiveTimeout();
                json["sendTimeout"] = zmqTcpConnectivityWidgetTmp->getSendTimeout();
                json["sendMessagesBufferSize"] = zmqTcpConnectivityWidgetTmp->getSendMessagesBufferSize();
                json["socketType"] = zmqTcpConnectivityWidgetTmp->getSocketType();
            }
        }
    }
}


void MainWindow::configTypeHandler(QWidget *node, const QString &configType)
{
    int configTypeInputRow;
    int configTypeInputColumn;
    int configTypeInputRowSpan;
    int configTypeInputColumnSpan;

    this->centralWidgetLayout->getItemPosition(
        this->centralWidgetLayout->indexOf(node),
        &configTypeInputRow,
        &configTypeInputColumn,
        &configTypeInputRowSpan,
        &configTypeInputColumnSpan
    );

    this->resetNode(configTypeInputRow, configTypeInputColumn);

    if ("" == configType) {
        int lastRow = this->centralWidgetLayout->filledRowCount() - 1;

        if (lastRow == configTypeInputRow) {
            return;
        }

        QLayoutItem *item = this->centralWidgetLayout->itemAtPosition(lastRow, 0);

        if (nullptr != item) {
            auto comboBox = dynamic_cast<QComboBox*>(item->widget());

            if (nullptr != comboBox && "" == comboBox->currentText()) {
                this->removeRow(lastRow);
            }
        }

        return;
    }

    if ("runConfig" == configType) {
        auto runConfigInput = new QComboBox;

        runConfigInput->addItems(
            {
                "",
                "scan",
                "targetThreshold",
                "targetTot",
                "targetCharge",
                "hwController",
                "chipConfigs",
                "connectivityRemotePull",
                "connectivityRemotePush",
                "connectivitySlaveProcess"
            }
        );

        QObject::connect(
            runConfigInput,
            &QComboBox::currentTextChanged,
            [this, runConfigInput](const QString &runConfig)->void
                {
                    this->runConfigHandler(runConfigInput, runConfig);
                }
        );

        this->addBranch(configTypeInputRow, configTypeInputColumn, runConfigInput);

        while (nullptr != this->centralWidgetLayout->itemAtPosition(configTypeInputRow + 2, configTypeInputColumn)) {
            this->destroyNode(configTypeInputRow + 2, configTypeInputColumn);
        }
    } else if (
        QStringList({"ScanSlave", "ProcessingSlave", "HistogrammingSlave", "AnalysisSlave"}).contains(configType)
    ) {
        auto connectivityRoleInput = new QComboBox;

        connectivityRoleInput->addItems({"", "connectivityMasterProcess"});

        QObject::connect(
            connectivityRoleInput,
            &QComboBox::currentTextChanged,
            [this, connectivityRoleInput](const QString &configType)->void
                {
                    this->connectivityRoleHandler(connectivityRoleInput, configType);
                }
        );

        this->addBranch(configTypeInputRow, configTypeInputColumn, connectivityRoleInput);

        if (nullptr == this->centralWidgetLayout->itemAtPosition(configTypeInputRow + 2, 0)) {
            this->addConfigTypeInput();
        }
    }
}

void MainWindow::runConfigHandler(QWidget *node, const QString &runConfig)
{
    int nodeRow, nodeColumn, nodeRowSpan, nodeColumnSpan;

    this->centralWidgetLayout->getItemPosition(
        this->centralWidgetLayout->indexOf(node), &nodeRow, &nodeColumn, &nodeRowSpan, &nodeColumnSpan
    );

    this->resetNode(nodeRow, nodeColumn);

    if ("" == runConfig) {
        QLayoutItem *item = this->centralWidgetLayout->itemAtPosition(nodeRow + 1, nodeColumn);

        if (nullptr != item && "" == dynamic_cast<QComboBox*>(item->widget())->currentText()) {
            this->removeRow(nodeRow + 1);
        }

        return;
    }

    if ("scan" == runConfig) {
        this->scanHandler(node, runConfig);
    } else if (QStringList({"targetThreshold", "targetTot", "targetCharge"}).contains(runConfig)) {
        this->tuningParameterHandler(node, runConfig);
    } else if ("hwController" == runConfig) {
        this->hwControllerHandler(node, runConfig);
    } else if ("chipConfigs" == runConfig) {
        this->chipConfigsHandler(node, runConfig);
    } else if (
        QStringList(
                {"connectivityRemotePull", "connectivityRemotePush", "connectivitySlaveProcess"}
            ).contains(runConfig)
    ) {
        this->connectivityRoleHandler(node, runConfig);
    }

    if (nullptr == this->centralWidgetLayout->itemAtPosition(nodeRow + 2, nodeColumn)) {
        auto freshRunConfigInput = new QComboBox;

        freshRunConfigInput->addItems(
            {
                "",
                "scan",
                "targetThreshold",
                "targetTot",
                "targetCharge",
                "hwController",
                "chipConfigs",
                "connectivityRemotePull",
                "connectivityRemotePush",
                "connectivitySlaveProcess"
            }
        );

        QObject::connect(
            freshRunConfigInput,
            &QComboBox::currentTextChanged,
            [this, freshRunConfigInput](const QString &runConfig)->void
                {
                    this->runConfigHandler(freshRunConfigInput, runConfig);
                }
        );

        this->insertRow(nodeRow + 1);
        this->centralWidgetLayout->addWidget(freshRunConfigInput, nodeRow + 2, nodeColumn);
    }
}

void MainWindow::scanHandler(QWidget *node, const QString &scan)
{
    int nodeRow, nodeColumn, nodeRowSpan, nodeColumnSpan;

    this->centralWidgetLayout->getItemPosition(
        this->centralWidgetLayout->indexOf(node), &nodeRow, &nodeColumn, &nodeRowSpan, &nodeColumnSpan
    );

    this->resetNode(nodeRow, nodeColumn);

    this->addBranch(nodeRow, nodeColumn, new ScanWidget);
}

void MainWindow::tuningParameterHandler(QWidget *node, const QString &tuningParameter)
{
    int runConfigInputRow;
    int runConfigInputColumn;
    int runConfigInputRowSpan;
    int runConfigInputColumnSpan;

    this->centralWidgetLayout->getItemPosition(
        this->centralWidgetLayout->indexOf(node),
        &runConfigInputRow,
        &runConfigInputColumn,
        &runConfigInputRowSpan,
        &runConfigInputColumnSpan
    );

    this->resetNode(runConfigInputRow, runConfigInputColumn);

    this->addBranch(runConfigInputRow, runConfigInputColumn, new TuningParameterWidget(tuningParameter));
}

void MainWindow::hwControllerHandler(QWidget *node, const QString &hwController)
{
    int runConfigInputRow;
    int runConfigInputColumn;
    int runConfigInputRowSpan;
    int runConfigInputColumnSpan;

    this->centralWidgetLayout->getItemPosition(
        this->centralWidgetLayout->indexOf(node),
        &runConfigInputRow,
        &runConfigInputColumn,
        &runConfigInputRowSpan,
        &runConfigInputColumnSpan
    );

    this->resetNode(runConfigInputRow, runConfigInputColumn);

    this->addBranch(runConfigInputRow, runConfigInputColumn, new HwControllerWidget);
}

void MainWindow::chipConfigsHandler(QWidget *node, const QString &chipConfigs)
{
    int runConfigInputRow;
    int runConfigInputColumn;
    int runConfigInputRowSpan;
    int runConfigInputColumnSpan;

    this->centralWidgetLayout->getItemPosition(
        this->centralWidgetLayout->indexOf(node),
        &runConfigInputRow,
        &runConfigInputColumn,
        &runConfigInputRowSpan,
        &runConfigInputColumnSpan
    );

    this->resetNode(runConfigInputRow, runConfigInputColumn);

    this->addBranch(runConfigInputRow, runConfigInputColumn, new ChipConfigsWidget);
}

void MainWindow::connectivityRoleHandler(QWidget *node, const QString &connectivityRole)
{
    int nodeRow, nodeColumn, nodeRowSpan, nodeColumnSpan;

    this->centralWidgetLayout->getItemPosition(
        this->centralWidgetLayout->indexOf(node), &nodeRow, &nodeColumn, &nodeRowSpan, &nodeColumnSpan
    );

    this->resetNode(nodeRow, nodeColumn);

    if ("" == connectivityRole) {
        return;
    } else if ("connectivityMasterProcess" == connectivityRole) {
        auto connectivityTypeInput = new QComboBox;
        connectivityTypeInput->addItems({"", "ZmqTcpConnectivity"});

        QObject::connect(
            connectivityTypeInput,
            &QComboBox::currentTextChanged,
            [this, connectivityTypeInput](const QString &connectivityType)->void
                {
                    this->connectivityTypeHandler(connectivityTypeInput, connectivityType);
                }
        );

        this->addBranch(nodeRow, nodeColumn, connectivityTypeInput);
    } else if (
            QStringList({"connectivityRemotePull", "connectivityRemotePush", "connectivitySlaveProcess"}
        ).contains(connectivityRole)
    ) {
        auto connectivityTargetInput = new QComboBox;
        connectivityTargetInput->addItems({"", "ScanSlave", "ProcessingSlave", "HistogrammingSlave", "AnalysisSlave"});

        QObject::connect(
            connectivityTargetInput,
            &QComboBox::currentTextChanged,
            [this, connectivityTargetInput](const QString &connectivityType)->void
                {
                    this->connectivityTargetHandler(connectivityTargetInput, connectivityType);
                }
        );

        this->addBranch(nodeRow, nodeColumn, connectivityTargetInput);
    }
}

void MainWindow::connectivityTargetHandler(QWidget *node, const QString &connectivityTarget)
{
    int nodeRow, nodeColumn, nodeRowSpan, nodeColumnSpan;

    this->centralWidgetLayout->getItemPosition(
        this->centralWidgetLayout->indexOf(node), &nodeRow, &nodeColumn, &nodeRowSpan, &nodeColumnSpan
    );

    this->resetNode(nodeRow, nodeColumn);

    if ("" == connectivityTarget) {
        QLayoutItem *item = this->centralWidgetLayout->itemAtPosition(nodeRow + 1, nodeColumn);

        if (nullptr != item && "" == dynamic_cast<QComboBox*>(item->widget())->currentText()) {
            this->removeRow(nodeRow + 1);
        }

        return;
    }

    auto connectivityTypeInput = new QComboBox;
    connectivityTypeInput->addItems({"", "ZmqTcpConnectivity"});

    QObject::connect(
        connectivityTypeInput,
        &QComboBox::currentTextChanged,
        [this, connectivityTypeInput](const QString &connectivityType)->void
            {
                this->connectivityTypeHandler(connectivityTypeInput, connectivityType);
            }
    );

    this->addBranch(nodeRow, nodeColumn, connectivityTypeInput);

    if (nullptr == this->centralWidgetLayout->itemAtPosition(nodeRow + 2, nodeColumn)) {
        auto freshConnectivityTargetInput = new QComboBox;

        freshConnectivityTargetInput->addItems(
            {"", "ScanSlave", "ProcessingSlave", "HistogrammingSlave", "AnalysisSlave"}
        );

        QObject::connect(
            freshConnectivityTargetInput,
            &QComboBox::currentTextChanged,
            [this, freshConnectivityTargetInput](const QString &runConfig)->void
                {
                    this->connectivityTargetHandler(freshConnectivityTargetInput, runConfig);
                }
        );

        this->insertRow(nodeRow + 1);
        this->centralWidgetLayout->addWidget(freshConnectivityTargetInput, nodeRow + 2, nodeColumn);
    }
}

void MainWindow::connectivityTypeHandler(QWidget *node, const QString &connectivityType)
{
    int connectivityTypeInputRow;
    int connectivityTypeInputColumn;
    int connectivityTypeInputRowSpan;
    int connectivityTypeInputColumnSpan;

    this->centralWidgetLayout->getItemPosition(
        this->centralWidgetLayout->indexOf(node),
        &connectivityTypeInputRow,
        &connectivityTypeInputColumn,
        &connectivityTypeInputRowSpan,
        &connectivityTypeInputColumnSpan
    );

    this->resetNode(connectivityTypeInputRow, connectivityTypeInputColumn);

    if ("" == connectivityType) {
        return;
    }

    if ("ZmqTcpConnectivity" == connectivityType) {
        this->addBranch(connectivityTypeInputRow, connectivityTypeInputColumn, new ZmqTcpConnectivityWidget);
    }
}

void MainWindow::insertRow(int afterRow)
{
    int rowCount = this->centralWidgetLayout->filledRowCount();
    int columnCount = this->centralWidgetLayout->columnCount();

    QLayoutItem *itemTmp;

    for (int row = rowCount - 1; row > afterRow; --row) {
        for (int column = 0; column < columnCount; ++column) {
            itemTmp = this->centralWidgetLayout->itemAtPosition(row, column);

            if (nullptr == itemTmp) {
                continue;
            }

            this->centralWidgetLayout->removeItem(itemTmp);
            this->centralWidgetLayout->addItem(itemTmp, row + 1, column);
        }
    }
}

void MainWindow::removeRow(int removeRow)
{
    int columnCount = this->centralWidgetLayout->columnCount();
    QLayoutItem *itemTmp;

    for (int column = 0; column < columnCount; ++column) {
        itemTmp = this->centralWidgetLayout->itemAtPosition(removeRow, column);

        if (nullptr == itemTmp) {
            continue;
        }

        this->centralWidgetLayout->removeItem(itemTmp);
        delete itemTmp->widget();
        delete itemTmp;
    }

    int rowCount = this->centralWidgetLayout->filledRowCount();

    for (int row = removeRow + 1; row < rowCount; ++row) {
        for (int column = 0; column < columnCount; ++column) {
            itemTmp = this->centralWidgetLayout->itemAtPosition(row, column);

            if (nullptr == itemTmp) {
                continue;
            }

            this->centralWidgetLayout->removeItem(itemTmp);
            this->centralWidgetLayout->addItem(itemTmp, row - 1, column);
        }
    }
}

void MainWindow::addBranch(int nodeRow, int nodeColumn, QWidget *branch)
{
    QLayoutItem *layoutItem = this->centralWidgetLayout->itemAtPosition(nodeRow, nodeColumn);

    if (nullptr == layoutItem || layoutItem->isEmpty()) {
        std::string errorMessage = __PRETTY_FUNCTION__ + std::string(": trying to add branch to empty node");

        throw std::logic_error(errorMessage);
    }

    this->insertRow(nodeRow);
    this->centralWidgetLayout->addWidget(branch, nodeRow + 1, nodeColumn + 1);
}

void MainWindow::resetNode(int nodeRow, int nodeColumn)
{
    if (nullptr == this->centralWidgetLayout->itemAtPosition(nodeRow, nodeColumn)) {
        std::string errorMessage = __PRETTY_FUNCTION__ + std::string(": trying to reset empty node");

        throw std::logic_error(errorMessage);
    }

    for (
        QLayoutItem *branch = this->centralWidgetLayout->itemAtPosition(nodeRow + 1, nodeColumn + 1);
        nullptr != branch;
        branch = this->centralWidgetLayout->itemAtPosition(nodeRow + 1, nodeColumn + 1)
    ) {
        this->destroyNode(nodeRow + 1, nodeColumn + 1);
    }
}

void MainWindow::destroyNode(int nodeRow, int nodeColumn)
{
    if (nullptr == this->centralWidgetLayout->itemAtPosition(nodeRow, nodeColumn)) {
        std::string errorMessage = __PRETTY_FUNCTION__ + std::string(": trying to destroy empty node");

        throw std::logic_error(errorMessage);
    }

    for (
        QLayoutItem *branch = this->centralWidgetLayout->itemAtPosition(nodeRow + 1, nodeColumn + 1);
        nullptr != branch;
        branch = this->centralWidgetLayout->itemAtPosition(nodeRow + 1, nodeColumn + 1)
    ) {
        this->destroyNode(nodeRow + 1, nodeColumn + 1);
    }

    this->removeRow(nodeRow);
}

void MainWindow::fileActionSlot(QAction *action)
{
    if ("Save as" == action->text()) {
        this->saveAsActionHandler();
    }
}
