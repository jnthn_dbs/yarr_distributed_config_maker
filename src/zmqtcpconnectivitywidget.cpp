#include "inc/zmqtcpconnectivitywidget.hpp"

ZmqTcpConnectivityWidget::ZmqTcpConnectivityWidget(QWidget *parent)
    : QWidget(parent)
{
    this->inputIpAddress = new QLineEdit("255.255.255.255", this);
    this->inputIpAddress->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    QRegExp ipAddressRegExp(
        QString("(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])")
            + QString("\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])")
            + QString("\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])")
            + QString("\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])")
    );

    auto ipAddressValidator = new QRegExpValidator(ipAddressRegExp, this);
    this->inputIpAddress->setValidator(ipAddressValidator);

    this->inputPort = new QSpinBox(this);
    this->inputPort->setMaximum(std::numeric_limits<uint16_t>::max());
    this->inputPort->setValue(65535);

    this->inputBind = new QComboBox(this);
    this->inputBind->addItems({"false", "true"});
    this->inputBind->setCurrentIndex(1);

    this->inputReceiveTimeout = new QSpinBox(this);
    this->inputReceiveTimeout->setMinimum(-1);
    this->inputReceiveTimeout->setMaximum(std::numeric_limits<int>::max());
    this->inputReceiveTimeout->setValue(10000);

    this->inputSendTimeout = new QSpinBox(this);
    this->inputSendTimeout->setMinimum(-1);
    this->inputSendTimeout->setMaximum(std::numeric_limits<int>::max());
    this->inputSendTimeout->setValue(10000);

    this->inputSendMessagesBufferSize = new QSpinBox(this);
    this->inputSendMessagesBufferSize->setMinimum(1);
    this->inputSendMessagesBufferSize->setMaximum(std::numeric_limits<int>::max());
    this->inputSendMessagesBufferSize->setValue(65536);

    this->inputSocketType = new QComboBox(this);
    this->inputSocketType->addItems(
        {
            "ZMQ_PAIR",
            "ZMQ_PUB",
            "ZMQ_SUB",
            "ZMQ_REQ",
            "ZMQ_REP",
            "ZMQ_DEALER",
            "ZMQ_ROUTER",
            "ZMQ_PULL",
            "ZMQ_PUSH",
            "ZMQ_XPUB",
            "ZMQ_XSUB",
            "ZMQ_STREAM"
        }
    );
    this->inputSocketType->setCurrentIndex(5);

    auto gridLayout = new QGridLayout(this);

    gridLayout->addWidget(new QLabel("ipAddress", this),              0, 0);
    gridLayout->addWidget(new QLabel("port", this),                   1, 0);
    gridLayout->addWidget(new QLabel("bind", this),                   2, 0);
    gridLayout->addWidget(new QLabel("receiveTimeout", this),         3, 0);
    gridLayout->addWidget(new QLabel("sendTimeout", this),            4, 0);
    gridLayout->addWidget(new QLabel("sendMessagesBufferSize", this), 5, 0);
    gridLayout->addWidget(new QLabel("socketType", this),             6, 0);

    gridLayout->addWidget(this->inputIpAddress,              0, 1);
    gridLayout->addWidget(this->inputPort,                   1, 1);
    gridLayout->addWidget(this->inputBind,                   2, 1);
    gridLayout->addWidget(this->inputReceiveTimeout,         3, 1);
    gridLayout->addWidget(this->inputSendTimeout,            4, 1);
    gridLayout->addWidget(this->inputSendMessagesBufferSize, 5, 1);
    gridLayout->addWidget(this->inputSocketType,             6, 1);

    this->setLayout(gridLayout);
}

ZmqTcpConnectivityWidget::~ZmqTcpConnectivityWidget()
{
    ;
}

std::string ZmqTcpConnectivityWidget::getIpAddress() const
{
    return this->inputIpAddress->text().toStdString();
}

uint16_t ZmqTcpConnectivityWidget::getPort() const
{
    return static_cast<uint16_t>(this->inputPort->value());
}

bool ZmqTcpConnectivityWidget::getBind() const
{
    return static_cast<bool>(this->inputBind->currentIndex());
}

int ZmqTcpConnectivityWidget::getReceiveTimeout() const
{
    return this->inputReceiveTimeout->value();
}

int ZmqTcpConnectivityWidget::getSendTimeout() const
{
    return this->inputSendTimeout->value();
}

int ZmqTcpConnectivityWidget::getSendMessagesBufferSize() const
{
    return this->inputSendMessagesBufferSize->value();
}

int ZmqTcpConnectivityWidget::getSocketType() const
{
    return this->inputSocketType->currentIndex();
}
