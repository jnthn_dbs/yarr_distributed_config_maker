#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// STL includes
#include <iostream>
#include <stdexcept>

// Qt includes
#include <QAction>
#include <QComboBox>
#include <QFileDialog>
#include <QMainWindow>
#include <QMenuBar>
#include <QMessageBox>
#include <QObject>
#include <QRegExp>
#include <QRegExpValidator>
#include <QScrollArea>
#include <QSize>
#include <QSizePolicy>
#include <QSpinBox>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QValidator>
#include <QWidget>

// 3rd party includes
#include "inc/json.hpp"

// custom includes
#include "inc/chipconfigswidget.hpp"
#include "inc/hwcontrollerwidget.hpp"
#include "inc/rowawaregridlayout.hpp"
#include "inc/scanwidget.hpp"
#include "inc/tuningparameterwidget.hpp"
#include "inc/zmqtcpconnectivitywidget.hpp"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    RowAwareGridLayout *centralWidgetLayout;

    void addConfigTypeInput();

    void saveAsActionHandler();
    void parseWidgetTree(int rowBegin, int rowEnd, int columnBegin, nlohmann::json &json);

    void configTypeHandler(QWidget *node, const QString &configType);
    void runConfigHandler(QWidget *node, const QString &runConfig);
    void tuningParameterHandler(QWidget *node, const QString &tuningParameter);
    void scanHandler(QWidget *node, const QString &scan);
    void hwControllerHandler(QWidget *node, const QString &hwController);
    void chipConfigsHandler(QWidget *node, const QString &chipConfigs);
    void connectivityRoleHandler(QWidget *node, const QString &connectivityRole);
    void connectivityTargetHandler(QWidget *node, const QString &connectivityTarget);
    void connectivityTypeHandler(QWidget *node, const QString &connectivityType);

    void insertRow(int afterRow);
    void removeRow(int removeRow);

    void addBranch(int nodeRow, int nodeColumn, QWidget *branch);
    void resetNode(int nodeRow, int nodeColumn);
    void destroyNode(int nodeRow, int nodeColumn);

private slots:
    void fileActionSlot(QAction*);
};

#endif // MAINWINDOW_H
