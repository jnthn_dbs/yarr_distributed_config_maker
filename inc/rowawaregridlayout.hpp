#ifndef ROWAWAREGRIDLAYOUT_HPP
#define ROWAWAREGRIDLAYOUT_HPP

// Qt includes
#include <QGridLayout>

class RowAwareGridLayout : public QGridLayout
{
public:
    RowAwareGridLayout(QWidget *parent = nullptr);
    ~RowAwareGridLayout() override;

    int filledRowCount();
};

#endif // ROWAWAREGRIDLAYOUT_HPP
