#ifndef CHIPCONFIGSWIDGET_HPP
#define CHIPCONFIGSWIDGET_HPP

// stl includes
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

// Qt includes
#include <QComboBox>
#include <QFileDialog>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QObject>
#include <QPushButton>
#include <QSpinBox>
#include <QStringList>
#include <QWidget>

class ChipConfigsWidget : public QWidget
{
Q_OBJECT

public:
    ChipConfigsWidget(QWidget *parent = nullptr);
    ~ChipConfigsWidget() override;

    std::string getChipType() const;
    int getMaskOpt() const;
    std::vector<std::string> getFilePathes() const;

private:
    QComboBox *chipTypeInput;
    QSpinBox *maskOptInput;
    QList<QLineEdit*> filePathesInputList;

    void addFilePathInput(const QString &filePath = QString());
};

#endif // CHIPCONFIGSWIDGET_HPP
