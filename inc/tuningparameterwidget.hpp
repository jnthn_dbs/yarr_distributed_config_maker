#ifndef TUNINGPARAMETERWIDGET_HPP
#define TUNINGPARAMETERWIDGET_HPP

// stl includes
#include <limits>

// Qt includes
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QWidget>

class TuningParameterWidget : public QWidget
{
Q_OBJECT

public:
    TuningParameterWidget(const QString &tuningParameter, QWidget *parent = nullptr);
    ~TuningParameterWidget() override;

    int getTuningParameter() const;

private:
    QSpinBox *tuningParameterInput;
};

#endif // TUNINGPARAMETERWIDGET_HPP
