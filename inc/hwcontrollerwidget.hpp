#ifndef HWCONTROLLERWIDGET_HPP
#define HWCONTROLLERWIDGET_HPP

// stl includes
#include <string>

// Qt includes
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QObject>
#include <QPushButton>
#include <QWidget>

class HwControllerWidget : public QWidget
{
Q_OBJECT

public:
    HwControllerWidget(QWidget *parent = nullptr);
    ~HwControllerWidget() override;

    std::string getControllerConfigFilePath() const;

private:
    QLineEdit *controllerConfigFilePathInput;
};

#endif // HWCONTROLLERWIDGET_HPP
