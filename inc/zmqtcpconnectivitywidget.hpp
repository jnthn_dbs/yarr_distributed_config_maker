#ifndef ZMQTCPCONNECTIVITYWIDGET_HPP
#define ZMQTCPCONNECTIVITYWIDGET_HPP

// stl includes
#include <limits>
#include <string>

// Qt includes
#include <QComboBox>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QWidget>

class ZmqTcpConnectivityWidget : public QWidget
{
Q_OBJECT

public:
    ZmqTcpConnectivityWidget(QWidget *parent = nullptr);
    ~ZmqTcpConnectivityWidget() override;

    std::string getIpAddress() const;
    uint16_t getPort() const;
    bool getBind() const;
    int getReceiveTimeout() const;
    int getSendTimeout() const;
    int getSendMessagesBufferSize() const;
    int getSocketType() const;

private:
    QLineEdit *inputIpAddress;
    QSpinBox *inputPort;
    QComboBox *inputBind;
    QSpinBox *inputReceiveTimeout;
    QSpinBox *inputSendTimeout;
    QSpinBox *inputSendMessagesBufferSize;
    QComboBox *inputSocketType;
};

#endif // ZMQTCPCONNECTIVITYWIDGET_HPP
