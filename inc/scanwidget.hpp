#ifndef SCANWIDGET_HPP
#define SCANWIDGET_HPP

// stl includes
#include <string>

// Qt includes
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QWidget>

class ScanWidget : public QWidget
{
Q_OBJECT

public:
    ScanWidget(QWidget *parent = nullptr);
    ~ScanWidget() override;

    std::string getType() const;

private:
    QLineEdit *typeInput;
};

#endif // SCANWIDGET_HPP
